import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  constructor( private back_location: Location,) { }

  ngOnInit() {
  }

  goBack(): void {
    this.back_location.back();
  }
}
