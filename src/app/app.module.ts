import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { ROUTING } from './routing';
import { AppComponent } from './app.component';
import { MaterialModule } from './material';

import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PendingPickingListComponent } from './pending-picking-list/pending-picking-list.component';
import { DetailsComponent } from './details/details.component';
import { ConfrimComponent } from './confrim/confrim.component';
import { PendingDescriptionComponent } from './pending-description/pending-description.component';
import { MoreInfoDialogComponent } from './more-info-dialog/more-info-dialog.component';
import { SearchDialogComponent } from './search-dialog/search-dialog.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    PendingPickingListComponent,
    DetailsComponent,
    ConfrimComponent,
    PendingDescriptionComponent,
    MoreInfoDialogComponent,
    SearchDialogComponent
  ],

  imports: [
    MaterialModule,ROUTING,RouterModule,Ng2SearchPipeModule,
    BrowserModule,BrowserAnimationsModule,FormsModule, ReactiveFormsModule
  ],
  entryComponents:[MoreInfoDialogComponent,ConfrimComponent,SearchDialogComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
