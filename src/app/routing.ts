import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core/src/metadata/ng_module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PendingPickingListComponent } from './pending-picking-list/pending-picking-list.component';
import { DetailsComponent } from './details/details.component';
import { PendingDescriptionComponent } from './pending-description/pending-description.component';

export const  AppRoutes: Routes = [
    { path: '', component: DashboardComponent },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'picking_list', component: PendingPickingListComponent },
    { path: 'picking_details', component: DetailsComponent },
    { path: 'picking_description', component: PendingDescriptionComponent },
    ]


export const ROUTING: ModuleWithProviders = RouterModule.forRoot(AppRoutes);
