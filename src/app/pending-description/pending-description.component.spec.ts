import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingDescriptionComponent } from './pending-description.component';

describe('PendingDescriptionComponent', () => {
  let component: PendingDescriptionComponent;
  let fixture: ComponentFixture<PendingDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
