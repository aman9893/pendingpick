import { Component, OnInit } from '@angular/core';
import {  MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
  selector: 'app-confrim',
  templateUrl: './confrim.component.html',
  styleUrls: ['./confrim.component.css']
})
export class ConfrimComponent implements OnInit {

  id: any;
  constructor(public dialogRef: MatDialogRef<ConfrimComponent>) { }

  ngOnInit() {
  
   }

  closeDialog(data) {
      this.dialogRef.close(data);
    
  }

  
}
