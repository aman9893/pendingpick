import { Component, OnInit, Inject } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-search-dialog',
  templateUrl: './search-dialog.component.html',
  styleUrls: ['./search-dialog.component.css']
})
export class SearchDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<SearchDialogComponent>) {}

    searchData= [];

    data=[{
      value:'MPV '
    },{
      value:'Aws File'
    },{
      value:'git File'
    },{
      value:'data file'
    },{
      value:'MPV Aws'
    },
    {
      value:'MPV Stess'
    },{
      value:'Stock'
    }]

  ngOnInit() {
  }

  close(){
    this.dialogRef.close();
  }
  checkboxdata(event){
      
     let F4Data = event.source.value;
     this.searchData.push(F4Data)
      console.log( this.searchData)
  }

 submit(){
     let data={
       value:this.searchData
     }

   this.dialogRef.close(this.searchData);
   } 
}
