import { Component, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-more-info-dialog',
  templateUrl: './more-info-dialog.component.html',
  styleUrls: ['./more-info-dialog.component.css']
})
export class MoreInfoDialogComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<MoreInfoDialogComponent>) {}

  ngOnInit() {
  }
  close(){
    this.dialogRef.close();
  }
}
