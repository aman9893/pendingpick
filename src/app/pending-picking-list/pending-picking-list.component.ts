import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import { MoreInfoDialogComponent } from '../more-info-dialog/more-info-dialog.component';
import { SearchDialogComponent } from '../search-dialog/search-dialog.component';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material';
import { NgForm, FormGroup, FormArray, FormsModule, FormControl, Validators, FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-pending-picking-list',
  templateUrl: './pending-picking-list.component.html',
  styleUrls: ['./pending-picking-list.component.css']
})
export class PendingPickingListComponent implements OnInit {
  dialogdataReturn: any;
  f4AddForm: FormGroup;

  constructor(public dialog: MatDialog,  private formBuilder: FormBuilder) { }

  ngOnInit() {
   this.createForm()
  }

  private createForm() {

    this.f4AddForm = this.formBuilder.group({
    
      w3data: new FormControl(),

    });
  }

moreinfo(event){
  event.stopPropagation()
  const dialogRef = this.dialog.open(MoreInfoDialogComponent, {
    width: '450px',
    data: 'data',
    panelClass:'superviserDialog',
    autoFocus:false
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log(result);
  });
}

f4dialog(){
  // event.stopPropagation()
  const dialogRef = this.dialog.open(SearchDialogComponent, {
    width: '450px',
    data: 'data',
    panelClass:'superviserDialog',
    autoFocus:false
  });

  dialogRef.afterClosed().subscribe(result => {
    console.log(result); 
    this.dialogdataReturn=result;
    console.log(this.dialogdataReturn)

    for (var i = 0; i < this.dialogdataReturn.length; i++) { 
      console.log(this.dialogdataReturn[i])
      //  this.f4AddForm.controls['w3data'].setValue(this.dialogdataReturn[i])
    }

    
  });
}
}
