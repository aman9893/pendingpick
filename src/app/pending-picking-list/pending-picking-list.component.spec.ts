import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingPickingListComponent } from './pending-picking-list.component';

describe('PendingPickingListComponent', () => {
  let component: PendingPickingListComponent;
  let fixture: ComponentFixture<PendingPickingListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingPickingListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingPickingListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
